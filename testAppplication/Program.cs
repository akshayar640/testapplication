﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testAppplication
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> testList = new List<int> { 1, 2, 3, 4 };

            //Find index of first odd element

            Predicate<int> predicate1 = x => { return x == 3; };

            object result = testList.Find(predicate1);
            writeResult(result);
        }

        static bool simpleMethod(int i)
        {
            Console.WriteLine("This is a new feature branch");
            Console.WriteLine("This is the master branch");
            return false;
        }


        static void writeResult (object obj)
        {
            Console.WriteLine(obj);
            Console.ReadLine();
        }
    }
}
